<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>StepStone</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="assets/css/screen.css">
	<style type="text/css">
		ol {
			margin: 0 auto;
			width: 300px;
			padding: 0 0 0 50px
		}
		ol > li {
			margin-bottom: 10px;
		}
		ol ul {
			margin-left: 0px;
			padding-left: 20px;
			font-size: 13px;
			margin-bottom: 20px;
			margin-top: 10px;
		}

		@media screen and (max-width: 480px) {
			ol {
				width: auto;
				padding-left: 0;
			}
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-16">
				<br><br><br>

				<img src="assets/images/logoX2.png" alt="" class="center-block">

				<br><br><br>

				<ol>
					<li><a href="frontpage.php">Forside (inkl. mobil)</a>
						<ul>
							<li><a href="frontpage_alt.php">Alternativt indhold</a></li>
						</ul>
					</li>
					<li><a href="side.php">Side med content (inkl. mobil) (tidl. artikelside)</a></li>
					<li><a href="listing.php">Jobopslag (inkl. mobil)</a></li>
					<li><a href="search.php">Søgeresultat (inkl. mobil)</a>
						<br><br>
					</li>
					<li><b>NY!</b> <a href="article.php">Artikelside med content (inkl. mobil)</a></li>
					<li><b>NY!</b> <a href="articles.php">Artikeloversigt (inkl. mobil)</a></li>
					<li><b>NY!</b> <a href="subfront.php">Subforside (inkl. mobil)</a></li>
				</ol>
			</div>
		</div>
	</div>
</body>