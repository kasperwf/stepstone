<?php
	$jsModule = ''; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-article.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-3  article-sidebar  hidden-xs">

			<ul class="side-nav" role="navigation">
				<li><a href="">Styrker og kompetencer</a></li>
				<li class="active">
					<a href="" class="active">Jobstrategi og målsætninger</a>
					<ul>
						<li><a href="">Sub sub 1</a></li>
						<li><a href="" class="active">Aktivt menupunkt</a></li>
						<li><a href="">Sub sub 3</a></li>
						<li><a href="">Sub sub 4</a></li>
					</ul>
				</li>
				<li><a href="">Vil du være leder?</a></li>
				<li><a href="">Lønforhandling</a></li>
				<li><a href="">Konfliktløsning</a></li>
			</ul>

			<div class="sponsor  center-block  text-center">
				<p>i samarbejde med</p>
				<img src="testcontent/lederne.jpg" alt="" class="company-logo">
			</div>

		</div>
		<div class="col-sm-6">

			<div class="article  content  component--default">
				<h1 class="title">Job strategi</h1>

				<div class="manchet">
					<p>Vil du have succes med din jobsøgning, er det vigtigt, at du sætter dig et mål og udarbejder en strategi for, hvordan du vil nå målet. Når du sætter dig et konkret mål, får du et klart billede af, hvor du gerne vil hen. Du har noget at pejle efter og undgår dermed at lade dig styre af tilfældigheder eller at søge job i "øst og vest".</p>
				</div>

				<div class="section  component--default">

					<p>Din personlige afklaring og dine konkrete ønsker til jobbet er afgørende for det mål, du sætter dig. I områderne her finder du værktøjer og redskaber, der kan hjælpe dig med at finde frem til det mål, der definerer dit ønskejob.</p>

					<p><strong>Hvad er vigtigt for dig?</strong><br>
					Du kan bruge modellen nedenfor som hjælp til at formulere, hvordan dit ønskejob ser ud. Du skal forholde dig til, hvilke ønsker du har til dine arbejdsvilkår, til dine arbejdsopgaver og til virksomheden. Når dine ønsker til alle tre områder går op i en højere enhed, har du et billede af dit ønskejob.</p>

					<p><img class="center-block" src="testcontent/article-circles.jpg" alt="alt tekst"></p>

					<div class="content-block">
						<h3>Gode spørgsmål at stille sig selv</h3>

						<p><strong>Arbejdsvilkår</strong><br>
						</p>
						<ul>
							<li>Hvilke rammer er vigtige for dig i dit job?</li>

							<li>Hvilken størrelse har virksomheden? Og hvilken branche?</li>

							<li>Hvor mange timer og i hvilket tidsrum arbejder du?</li>

							<li>Hvor lang er afstanden mellem dit hjem og jobbet?</li>

							<li>Hvad er din forventning til lønnen og personalegoder?</li>

							<li>Hvilke arbejdsopgaver drømmer du om at beskæftige dig med?</li>
						</ul>

						<p><br>
						<strong>Arbejdsopgaver</strong><br>
						</p>
						<ul>
						<li>Hvilke ansvars- og funktionsområder har du?</li>

						<li>Hvilke faglige og personlige kompetencer har du i spil?</li>

						<li>Hvad er dine succeskriterier?</li>

						<li>Hvor høj grad af frihed har du til at løse dine opgaver?</li>
						</ul>

						<p><br>
						<strong>Virksomheden</strong><br>
						</p>
						<ul>
						<li>På hvilket niveau i organisationen er du placeret?</li>

						<li>Hvor mange medarbejdere eller ledere refererer til dig?</li>

						<li>Hvilken stillingsbetegnelse har du?</li>

						<li>Hvordan matcher dine personlige værdier virksomhedens værdier?</li>

						<li>Hvilke ønsker har du til personlig og faglig udvikling?</li>

						<li>Hvordan bliver du selv ledet?</li>
						</ul>
					</div>


					<p><br>
					Du skal også overveje hvilke andre relevante forhold, der er vigtige for dig i jobbet?<br>
					<br>
					Nu er du klar til at definere dit mål.<br>
					<br>
					Det er vigtigt, at du beskriver dit ønskejob - og dermed dit mål - så konkret, at du ikke bliver i tvivl om, hvad du går efter og indleder en slingrekurs.<br>
					<br>
					Eksempel på et mål. "Et job som marketingchef med ledelse af minimum fem medarbejdere inden for medicinalbranchen i Trekantsområdet senest om et år".<br>
					<br>
					Det er vigtigt, at du løbende vurderer dit mål, efterhånden som du får mere viden og indsigt. Mål er ikke statiske. Du skal følge dit jobmål indtil et nyt og bedre giver mere mening for dig.</p>

				</div>



			</div>

		</div>
		<div class="col-sm-3  listing-sidebar-right  hidden-xs">
			<div class="secondary-content  narrow  frontpage  js-hideonscroll">
				<button class="btn btn--create-cv">Opret CV</button>
				<button class="btn btn--add-job">Opret job</button>

				<a href="">
					<img src="testcontent/temp-ad.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
</div>


<?php require('_footer.php'); ?>