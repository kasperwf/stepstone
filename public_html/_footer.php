
		<!-- super column (the one to the right that never scales) -->
		<!-- // end super column -->
	</div> <!-- .site-content -->

	<div class="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">

					<div class="col-sm-9 ">
						<ul>
							<li><a href="">Sitemap</a></li>
							<li><a href="">Mobile site</a></li>
							<li><a href="">Cookies og Databeskyttelse</a></li>
							<li><a href="">Dine rettigheder</a></li>
							<li class="visible-xs"><a href="">Kontakt os</a></li>
							<li><a href="">Vilkår for anvendelse</a></li>
						</ul>
					</div>

					<div class="col-sm-3  hidden-xs">
						<div class="narrow">
							<ul class="social">
								<li><a href=""><img src="assets/images/social-fb.png" alt=""></a></li>
								<li><a href=""><img src="assets/images/social-linkedin.png" alt=""></a></li>
								<li><a href=""><img src="assets/images/social-gplus.png" alt=""></a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="footer-middle">
			<div class="container">
					<!-- mobile specific logo and network -->
					<div class="row visible-xs-block">
						<div class=" text-center  col-xs-5  col-xs-offset-1">
							<img src="assets/images/logoX2.png" class="footer-logo  img-responsive">
						</div>
						<div class="col-xs-5  text-center">
							<img src="assets/images/the-network.png" alt="" class="img-responsive">
						</div>
					</div>
					<!-- end of mobile specific logo and network -->
				<div class="row">

					<div class="col-sm-2   hidden-xs">
						<img src="assets/images/logoX2.png" class="footer-logo  img-responsive">
					</div>
					<div class="col-sm-7  col-xs-16  text">
						<p>Mere end 2,700 job gør det muligt for kandidater fra alle erhverv og industrier at finde det helt rigtige job på StepStone. Udover muligheden for at finde det perfekte job, tilbyder StepStone også værdifulde tips om arbejde, CV, jobsamtalen, ansøgning og jobsøgning.</p>
					</div>
					<div class="col-sm-3  text-center  hidden-xs">
						<div class="narrow">
							<img src="assets/images/the-network.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
			<!-- mobile social row -->
			<div class="visible-xs-block  social-row">
				<ul class="social">
					<li><a href=""><img src="assets/images/social-fb-dark.png" alt=""></a></li>
					<li><a href=""><img src="assets/images/social-linkedin-dark.png" alt=""></a></li>
					<li><a href=""><img src="assets/images/social-gplus-dark.png" alt=""></a></li>
				</ul>
			</div>
			<!-- end of mobile social row -->
			<div class="text-center">
				Copyright &copy; StepStone GmbH 1996-2014
			</div>
			</div>
		</div>
	</div>

	<!-- scripts here -->
	<script src="assets/js/vendor/jquery.min.js"></script>
	<script src="assets/js/vendor/bootstrap.min.js"></script>
	<?php if (isset($jsModule) && $jsModule != ''): ?>
		<?php if ($jsModule == 'frontpage'): ?>
			<!-- newsTicker script -->
			<script src="assets/js/modules/newsTicker.js"></script>
			<!-- // newsTicker script -->
		<?php endif; ?>
		<script src="assets/js/modules/<?php echo $jsModule; ?>.js"></script>
	<?php endif ?>
	<script src="assets/js/modules/_all.js"></script>
	<!-- // end scripts -->
</body>
</html>
