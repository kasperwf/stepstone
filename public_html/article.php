<?php
	$jsModule = ''; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-article.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-3  article-sidebar  hidden-xs">

			<ul class="side-nav" role="navigation">
				<li><a href="">Styrker og kompetencer</a></li>
				<li class="active">
					<a href="" class="active">Jobstrategi og målsætninger</a>
					<ul>
						<li><a href="">Sub sub 1</a></li>
						<li><a href="" class="active">Aktivt menupunkt</a></li>
						<li><a href="">Sub sub 3</a></li>
						<li><a href="">Sub sub 4</a></li>
					</ul>
				</li>
				<li><a href="">Vil du være leder?</a></li>
				<li><a href="">Lønforhandling</a></li>
				<li><a href="">Konfliktløsning</a></li>
			</ul>

		</div>
		<div class="col-sm-6">

			<div class="article  content  component--default">
				<h1 class="title">Undgå fejl på de sociale medier som jobsøgende</h1>

				<img src="testcontent/article-new-top.jpg" alt="" class="img-responsive">

				<div class="article-info">
					<time datetime="2014-08-01">1. august 2014</time>
					<div class="tags">
						<div class="tag">Sociale medier</div>
						<div class="tag">Jobsøgning</div>
					</div>
				</div>

				<div class="manchet">
					<p>Facebookbilleder, datingprofiler, blogs og indlæg på internetsider - alt dette er noget en arbejdsgiver kigger nærmere på, når de vil vide mere om en ansøger. Den umiddelbart amerikanske tendens er nemlig ved at udbrede sig i Danmark, og man kan som jobsøgende godt regne med, at ens liv på de sociale medier kommer til at spille en rolle, når arbejdsgiveren skal beslutte om man skal til samtale.</p>
					<p>Som jobsøgende er det derfor vigtigt at man bruger de sociale medier rigtigt, og hele tiden overvejer, om det billede man er i færd med at lægge op og den opdatering eller det tweet man er ved at skrive, er noget der vil trække én op eller ned i værdi, set fra arbejdsgiverens synspunkt. </p>
				</div>

				<div class="section  component--default">
					<p>Om man søger job indenfor HR, som manager, rengøringsjob eller studiejob er det vigtigt at markedsføre sig selv og holde sig til overfor virksomheder og organisationer på Twitter og LinkedIn.<br>
					Facebookbilleder, datingprofiler, blogs og indlæg på internetsider - alt dette er noget en arbejdsgiver kigger nærmere på, når de vil vide mere om en ansøger. Den umiddelbart amerikanske tendens er nemlig ved at udbrede sig i Danmark, og man kan som jobsøgende godt regne med, at ens liv på de sociale medier kommer til at spille en rolle, når arbejdsgiveren skal beslutte om man skal til samtale.</p>

					<p>Som jobsøgende er det derfor vigtigt at man bruger de sociale medier rigtigt, og hele tiden overvejer, om det billede man er i færd med at lægge op og den opdatering eller det tweet man er ved at skrive, er noget der vil trække én op eller ned i værdi, set fra arbejdsgiverens synspunkt. Om man søger job indenfor HR, som manager, rengøringsjob eller studiejob er det vigtigt at markedsføre sig selv og holde sig til overfor virksomheder og organisationer på Twitter og LinkedIn.</p>

					<p><br></p>

					<p><strong>Udnyt de sociale medier optimalt</strong></p>
					<p>I Danmark er Facebook og LinkedIn de mest udbredte og populære sociale medier, og Twitter er godt på vej. De bruges hver især til forskellige formål, men er alle blevet en fast del af mediebrugernes liv og hverdag - og dette har jobmarkedet og arbejdsgiveren opdaget. Er profilbilledet fra den seneste tur i byen, med en øl i hånden og et lidt for kækt smil er det derfor en god idé at overveje, om det er dette førstehåndsindtryk, du gerne vil give en arbejdsgiver. Læs derfor jobannoncerne grundigt og søg de jobs, der passer til den person du giver udtryk for at være.</p>
				</div>

			</div>

			<div class="imagelinkbox-component">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="title">Relaterede artikler</h2>
						<hr>
					</div>
				</div>
				<div class="row  mobile-carousel  related-articles">
					<div class="mobile-carousel-holder">
						<div class="col-sm-6   imagelinkbox">
							<img src="testcontent/article-new-related-1.jpg" alt="" class="img-responsive">
							<h4>Uligheden i Danmark er stigende</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
						<div class="col-sm-6  imagelinkbox">
							<img src="testcontent/article-new-related-2.jpg" alt="" class="img-responsive">
							<h4>Hvornår er det relevant for dig at søge nyt job</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-sm-3  listing-sidebar-right  hidden-xs">
			<div class="secondary-content  narrow  frontpage  js-hideonscroll">
				<button class="btn btn--create-cv">Opret CV</button>
				<button class="btn btn--add-job">Opret job</button>

				<a href="">
					<img src="testcontent/temp-ad.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
</div>


<?php require('_footer.php'); ?>