<?php
	$jsModule = 'search'; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-subpages.php'); ?>

<div class="container">
	<div class="row ">
		<div class="col-sm-3 search-sidebar">

			<h1 class="search-title  component--default  visible-xs-block">123 job <span>matcher</span> din søgning</h1>

			<div class="your-choices  component--lightblue" id="yourchoices">
				<p class="title" data-toggle="collapse" data-parent="#yourchoices" href="#mychoices">Dine valg</p>
				<div class="panel-collapse collapse in" id="mychoices">
					<div class="checkbox-holder">
						<input type="checkbox" name="area" value="1" id="copenhagen" checked>
						<label for="copenhagen">Københavnsområdet</label>
					</div>
					<div class="checkbox-holder">
						<input type="checkbox" name="position" id="farmer" value="2" checked>
						<label for="farmer">Landbrug &amp; Fiskeri</label>
					</div>
				</div>
			</div>

			<div class="filter 	component--lightblue" id="filtersholder">
				<p class="title collapsed" data-toggle="collapse" data-parent="#filtersholder" href="#filters">Filtrér din søgning</p>

				<div class="filters panel-collapse collapse" id="filters">

					<div class="border-box  component--default" id="filterarea">
						<p class="title" data-toggle="collapse" data-parent="#filterarea" href="#areas">Område</p>
						<div class="options-holder  panel-collapse  collapse  in" id="areas">
							<div class="is-selected">Københavnsområdet </div>
							<div class="">Region Sjælland (1)</div>
							<div class="">Fyn (2)</div>
							<div class="">Nordjylland (0)</div>
							<div class="">Midtjylland (5)</div>
							<div class="">Sydjylland (1)</div>
							<div class="">Bornholm (0)</div>
							<div class="">Skåne (0)</div>
							<div class="">Udlandet (3)</div>
							<div class="">Grønland og Færøerne (0)</div>
							<div class="">Uspecificeret arbejdssted (1)</div>
						</div>
					</div>

					<div class="border-box" id="filtercategory">
						<p class="title" data-toggle="collapse" data-parent="#filtercategory" href="#categories">Kategori</p>
						<div class="options-holder  panel-collapse  collapse  in" id="categories">
							<div class="">Administration / Kontor</div>
							<div class="">Design &amp; Arkitektur</div>
							<div class="">Human Resources</div>
							<div class="">Ingeniør</div>
							<div class="">Jura</div>
							<div class="is-selected">Landbrug &amp; Fiskeri</div>
							<div class="">Markedsføring &amp; PR</div>
							<div class="">Medicinal / Biotek</div>
							<div class="">Offentlige hverv</div>
							<div class="">Salg &amp; Indkøb</div>
							<div class="">Transport &amp; Logistik</div>
							<div class="">Økonomi, Regnskab &amp; Revision</div>
							<div class="">Bank, Finans &amp; Forsikring</div>
							<div class="">Hotel, Restaurant &amp; Turisme</div>
							<div class="">Industri, Håndværk &amp; Service</div>
							<div class="">IT</div>
							<div class="">Kunst &amp; Kultur</div>
							<div class="">Ledelse</div>
							<div class="">Medicin, Sundhed &amp; Omsorg</div>
							<div class="">Medie &amp; Information</div>
							<div class="">Olieindustri &amp; on-/offshore</div>
							<div class="">Sikkerhed &amp; Alarmberedskab</div>
							<div class="">Uddannelse / Børn &amp; Unge</div>
						</div>
					</div>
				</div>
			</div>


		</div>
		<div class="col-sm-6">

			<div class="search  component--default">
				<h1 class="search-title hidden-xs">123 job <span>matcher</span> din søgning</h1>

				<button class="btn--create-agent  btn-block  collapsed" id="toggleAgent">
					Opret jobagent og modtag relevante job direkte i din email
					<span>&nbsp;</span>
				</button>
				<div class="create-agent" style="display: none" id="createAgent">
					<form>
						<div class="row">
							<div class="col-sm-8">
								<input type="email" class="email  form-control" placeholder="Indtast emailadresse">
							</div>
							<div class="col-sm-4">
								<input type="submit" value="Opret jobagent" class="btn btn-primary btn-md btn-block">
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-sm-12">
							<p class="disclaimer">Du har lavet en jobsøgning på StepStone. Gem din søgning og modtag gratis matchende job via mail. Du skal blot indtaste din mail og du får relevante jobtilbud hver dag. Vi gemmer din Jobagent på din StepStone profil. Har du ikke en profil, vil der automatisk blive oprettet en til dig. For mere information se vores Erklæring om databeskyttelse. Hvis du ikke ønsker at modtage StepStone mails til lignende tjenester, skal du blot klikke på afmeld linket i mailen eller afmelde dem via din StepStone profil.</p>
						</div>
					</div>
				</div>

				<!-- results -->
				<div class="results  component--default">

					<!-- result TEXT ONLY (doesn't have a separate mobile version) -->
					<div class="result  result--text-only">
						<div class="table-row">
							<div class="left">
								<h3 class="title"><a href="">Oracle Safety / Pharmacovigilence Sales Consultant</a></h3>
								<div class="info">Oracle | Storkøbenhavn</div>
								<div class="description">
									<p>Oracle provides the world's most complete, open, and integrated business software and hardware systems, with more than 370,000...</p>

									<time datetime="2014-09-29" class="published">Publiceret 29.09.2014</time>

									<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
								</div>
							</div>
						</div>
					</div>
					<!-- end of result -->

					<!-- result -->
					<div class="result">
						<div class="table-row  hidden-xs">
							<div class="left">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
								<div class="info">HAYS | København</div>
								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat.</p>

									<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
								</div>
							</div>
							<div class="right">
								<img src="testcontent/hays.jpg" class="company-logo  img-responsive">
								<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
							</div>
						</div>

						<!-- mobile result -->
						<div class="visible-xs-block">
							<div class="title-row">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
							</div>
							<div class="logo-info-row">
								<div class="company-logo">
									<img src="testcontent/hays.jpg" class="img-responsive">
								</div>
								<div class="info">HAYS<br>København</div>
							</div>
							<div class="description">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.</p>

								<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
							</div>
							<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
						</div>
						<!-- end of mobile result -->
					</div>
					<!-- end of result -->

					<!-- result -->
					<div class="result">
						<div class="table-row  hidden-xs">
							<div class="left">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
								<div class="info">Telia | København</div>
								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat.</p>

									<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
								</div>
							</div>
							<div class="right">
								<img src="testcontent/telia.jpg" class="company-logo  img-responsive">
								<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
							</div>
						</div>

						<!-- mobile result -->
						<div class="visible-xs-block">
							<div class="title-row">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
							</div>
							<div class="logo-info-row">
								<div class="company-logo">
									<img src="testcontent/telia.jpg" class="img-responsive">
								</div>
								<div class="info">Telia<br>København</div>
							</div>
							<div class="description">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.</p>

								<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
							</div>
							<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
						</div>
						<!-- end of mobile result -->
					</div>
					<!-- end of result -->

					<!-- result -->
					<div class="result">
						<div class="table-row  hidden-xs">
							<div class="left">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
								<div class="info">Telia | København</div>
								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat.</p>

									<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
								</div>
							</div>
							<div class="right">
								<img src="testcontent/telia.jpg" class="company-logo  img-responsive">
								<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
							</div>
						</div>

						<!-- mobile result -->
						<div class="visible-xs-block">
							<div class="title-row">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
							</div>
							<div class="logo-info-row">
								<div class="company-logo">
									<img src="testcontent/telia.jpg" class="img-responsive">
								</div>
								<div class="info">Telia<br>København</div>
							</div>
							<div class="description">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.</p>

								<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
							</div>
							<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
						</div>
						<!-- end of mobile result -->
					</div>
					<!-- end of result -->

					<!-- result -->
					<div class="result">
						<div class="table-row  hidden-xs">
							<div class="left">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
								<div class="info">Telia | København</div>
								<div class="description">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat.</p>

									<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
								</div>
							</div>
							<div class="right">
								<img src="testcontent/telia.jpg" class="company-logo  img-responsive">
								<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
							</div>
						</div>

						<!-- mobile result -->
						<div class="visible-xs-block">
							<div class="title-row">
								<h3 class="title"><a href="">PA to the General Counsel</a></h3>
							</div>
							<div class="logo-info-row">
								<div class="company-logo">
									<img src="testcontent/telia.jpg" class="img-responsive">
								</div>
								<div class="info">Telia<br>København</div>
							</div>
							<div class="description">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat.</p>

								<time datetime="2015-05-21" class="published">Publiceret 21.05.2014</time>
							</div>
							<a href="" class="btn btn-primary btn-sm btn-block">Se job</a>
						</div>
						<!-- end of mobile result -->
					</div>
					<!-- end of result -->

				</div>
				<!-- end of results -->

				<!-- result pagination -->
				<div class="result-pagination  component--default">
					<div class="pull-left">
						Viser 1-5 af 21 job
					</div>
					<div class="pull-right">
						<ul class="pagination-nav">
							<li class="previous disabled">
								<a>&laquo;</a>
							</li>
							<li class="current"><a href="#page=1">1</a></li>
							<li><a href="#page=2">2</a></li>
							<li><a href="#page=3">3</a></li>
							<li><a href="#page=4">4</a></li>
							<li><a href="#page=5">5</a></li>
							<li class="next">
								<a href="">&raquo;</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- end of result pagination -->
			</div>
		</div>
		<div class="col-sm-3  listing-sidebar-right  hidden-xs">
			<div class="secondary-content  narrow  frontpage  js-hideonscroll">
				<button class="btn btn--create-cv">Opret CV</button>
				<button class="btn btn--add-job">Opret job</button>

				<a href="">
					<img src="testcontent/temp-ad.jpg" alt="">
				</a>
			</div>
		</div>

	</div>
</div>


<?php require('_footer.php'); ?>