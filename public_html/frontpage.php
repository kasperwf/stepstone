<?php
	$jsModule = 'frontpage'; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-frontpage.php'); ?>

<!-- category listing -->
<div class="category-component">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">

				<div class="row">
					<div class="col-sm-12 ">
						<h2 class="title  js-mobile-expander" data-target=".categories.row">Find job per <span class="color--primary">kategori</span></h2>
						<hr>
					</div>
				</div>

				<div class="row  categories">
					<div class="col-sm-4 ">
						<ul>
							<li><a href="" class="no-children">Administration / Kontor</a></li>
							<li><a href="" class="no-children">Design &amp; Arkitektur</a></li>
							<li><a href="" class="no-children">Human Resources</a></li>
							<li><a href="" class="no-children">Ingeniør</a></li>
							<li><a href="" class="no-children">Jura</a></li>
							<li><a href="" class="no-children">Landbrug &amp; Fiskeri</a></li>
							<li><a href="" class="no-children">Markedsføring &amp; PR</a></li>
							<li><a href="" class="no-children">Medicinal / Biotek</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<ul>
						<li><a href="" class="no-children">Offentlige hverv</a></li>
						<li><a href="" class="no-children">Salg &amp; Indkøb</a></li>
						<li><a href="" class="no-children">Transport &amp; Logistik</a></li>
						<li><a href="" class="no-children">Økonomi, Regnskab &amp; Revision</a></li>
						<li><a href="" class="no-children">Bank, Finans &amp; Forsikring</a></li>
						<li><a href="" class="no-children">Hotel, Restaurant &amp; Turisme</a></li>
						<li><a href="" class="no-children">Industri, Håndværk &amp; Service</a></li>
						<li><a href="" class="no-children">IT</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<ul>
						<li><a href="" class="no-children">Kunst &amp; Kultur</a></li>
						<li><a href="" class="no-children">Ledelse</a></li>
						<li><a href="" class="no-children">Medicin, Sundhed &amp; Omsorg</a></li>
						<li><a href="" class="no-children">Medie &amp; Information</a></li>
						<li><a href="" class="no-children">Olieindustri &amp; on-/offshore</a></li>
						<li><a href="" class="no-children"> Sikkerhed &amp; Alarmberedskab</a></li>
						<li><a href="" class="no-children">Uddannelse / Børn &amp; Unge</a></li>
						</ul>
					</div>
				</div>

			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="ad-block  ad-block--frontpage  secondary-content  narrow">
					<a href="">
						<img src="testcontent/temp-ad.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- // category listing -->


<!-- the 4 image boxes with text and read-more link -->
<div class="imagelinkbox-component">
	<div class="container">

		<div class="row  header">
			<div class="col-sm-7 ">
				<h2 class="title">Gode råd til jobsøgning og karriere</h2>
			</div>
			<div class="col-sm-2  text-right  hidden-xs">
				<a href="">Se alle karriereråd</a>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-9"><hr></div>
		</div>

		<div class="mobile-carousel">
			<div class="row mobile-carousel-holder">
				<div class="col-sm-3   imagelinkbox">
					<img src="testcontent/advice-1.jpg" alt="" class="img-responsive">
					<h4>Jobstrategi</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3  imagelinkbox">
					<img src="testcontent/advice-2.jpg" alt="" class="img-responsive">
					<h4>Dit nye job er kun 5 skridt væk!</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3  imagelinkbox">
					<img src="testcontent/advice-3.jpg" alt="" class="img-responsive">
					<h4>Jeg er ikke som de andre</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3  hidden-xs">
					<!-- Frontpage overlap from ad-banner -->
					&nbsp;
				</div>
			</div>
		</div>

	</div>
</div>
<!-- //the 4 image boxes with text and read-more link -->


<!-- video and forum shortcuts -->
<div class="container  component--default">
	<div class="row">
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-6 ">
					<!-- video iframe -->
					<iframe width="380" height="213" src="//www.youtube.com/embed/QmsRgzfr0sk?fs=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;color=white&amp;theme=light" frameborder="0" allowfullscreen></iframe>
					<!-- end ofvideo iframe -->
				</div>
				<div class="col-sm-6">
					<div class="component--lightblue">

						<h2>Find dit jobfora</h2>

						<hr>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

						<div class="row">
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Salg og marketing</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ingeniør og teknik</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Økonomi og finans</a>
							</div>
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Medicinal og biotek</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ledelse</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">IT</a>
							</div>
							<div class="col-sm-12 visible-xs">
							<a href="" class="btn  btn-primary  btn-md  btn-block">Find jobfora</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3  hidden-xs">&nbsp;</div>
	</div>
</div>
<!-- // video and forum shortcuts -->


<!-- carousel -->
<?php require('_carousel.php'); ?>
<!-- // carousel -->


<?php require('_footer.php'); ?>
