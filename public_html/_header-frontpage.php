<?php require("__head.php"); ?>

		<!-- Top bar (Langugage to the left, Feedback and Security links to the right) -->
		<div class="topbar  hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-9   topbar-left">

					</div>
					<div class="col-sm-3">

						<div class="topbar-small-links  narrow">
							<a href="">Send feedback</a>
							<a href="">Sikkerhed</a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end top bar -->

		<!-- Nav bar Logo || Nav -->
		<div class="navbar--custom  js-header  hidden-xs" role="navigation">

			<div class="container">
				<div class="row">

					<div class="col-sm-2 ">
						<a class="logo-holder" href="#">
							<img src="assets/images/logoX2.png" class="nav-logo  img-responsive">
						</a>
					</div>

					<div class="col-sm-7">
						<ul class="nav  js-nav--primary">
							<li class="active  current-page"><a href="#" data-id="jobsearch">Jobsøger</a></li>
							<li><a href="#" data-id="company">Virksomhed</a></li>
							<li><a href="#" data-id="about">Om os</a></li>
						</ul>
					</div>

					<div class="col-sm-3">
						<div class="narrow">
							<button class="btn btn--login">Log ind</button>
						</div>
					</div>

				</div>
			</div>
			<div class="nav--secondary  js-nav--secondary">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 ">

							<ul data-id="jobsearch">
								<li><a href="">Søg job</a></li>
								<li class="active"><a href="">Jobsøgning og karriere</a></li>
								<li><a href="">Jobfora</a></li>
								<li><a href="">Lorem ipsum</a></li>
								<li><a href="">Dolores est</a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end nav bar -->

		<!-- Search component -->
		<div class="search-component  search-component--frontpage  js-search">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">

						<form class="form-horizontal" role="form" action="" method="">

							<div class="row  js-hideonscroll  hide-on-collapsed">
								<div class="col-sm-12  component-header">
									<h1 class="pull-left">Find dit <span class="color--primary">karriere-job</span> her</h1>
									<button class="btn  btn-link  component-button--advanced  color--white  pull-right"><span class="glyphicon  glyphicon-search  color--primary "></span> Avanceret søgning</button>
								</div>
							</div>

							<div class="row  js-hideonscroll  hide-on-collapsed">
								<div class="col-sm-12">
									<div class="divider"><hr></div>
								</div>
							</div>

							<div class="row  form-group  component-fields">
									<div class="col-sm-5 ">
										<label for="" class="control-label  text-left  hidden-xs">Hvad</label>
										<input type="text" class="form-control" placeholder="Titel, virksomhed eller søgeord">
									</div>

									<div class="col-sm-5">
										<label for="" class="control-label  text-left  hidden-xs">Hvor</label>
										<input type="text" class="form-control" placeholder="By eller postnummer">
									</div>

									<div class="col-sm-2">
										<button type="submit" class="btn  btn-success  btn-block">SØG</button>
									</div>

							</div>
						</form>

					</div>
					<div class="col-sm-3  hidden-xs">

						<div class="secondary-content  narrow  frontpage  js-hideonscroll">
							<button class="btn btn--create-cv">Opret CV</button>
							<button class="btn btn--add-job">Opret job</button>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end search component -->
