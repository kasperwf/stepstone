<?php
	$jsModule = ''; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-article.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-3  article-sidebar  hidden-xs">

			<ul class="side-nav" role="navigation">
				<li><a href="">Styrker og kompetencer</a></li>
				<li class="active">
					<a href="" class="active">Jobstrategi og målsætninger</a>
					<ul>
						<li><a href="">Sub sub 1</a></li>
						<li><a href="" class="active">Aktivt menupunkt</a></li>
						<li><a href="">Sub sub 3</a></li>
						<li><a href="">Sub sub 4</a></li>
					</ul>
				</li>
				<li><a href="">Vil du være leder?</a></li>
				<li><a href="">Lønforhandling</a></li>
				<li><a href="">Konfliktløsning</a></li>
			</ul>

		</div>
		<div class="col-sm-6">

			<!-- Top box -->
			<div class="row">
				<div class="col-sm-12  imagelinkbox  imagelinkbox--large  articles-page">
					<img src="testcontent/articles-3.jpg" alt="" class="img-responsive">
					<h4>Hvordan får du gang i en international karriere?</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
			</div>

			<!-- Small boxes -->
			<div class="image-boxes">
				<div class="row  imagelinkbox-component  mobile-carousel  articles-page">
					<hr>
					<div class="mobile-carousel-holder">
						<div class="col-sm-6   imagelinkbox">
							<img src="testcontent/articles-3.jpg" alt="" class="img-responsive">
							<h4>Hvordan får du gang i en international karriere?</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
						<div class="col-sm-6  imagelinkbox">
							<img src="testcontent/articles-4.jpg" alt="" class="img-responsive">
							<h4>Skal du være ven med din chef på Facebook?</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
					<hr>
						<div class="col-sm-6   imagelinkbox  clear-left">
							<img src="testcontent/article-new-related-1.jpg" alt="" class="img-responsive">
							<h4>Uligheden i Danmark er stigende</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
						<div class="col-sm-6  imagelinkbox">
							<img src="testcontent/article-new-related-2.jpg" alt="" class="img-responsive">
							<h4>Hvornår er det relevant for dig at søge nyt job</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-sm-3  listing-sidebar-right  hidden-xs">
			<div class="secondary-content  narrow  frontpage  js-hideonscroll">
				<button class="btn btn--create-cv">Opret CV</button>
				<button class="btn btn--add-job">Opret job</button>

				<a href="">
					<img src="testcontent/temp-ad.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
</div>


<?php require('_footer.php'); ?>