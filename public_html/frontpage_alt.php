<?php
	$jsModule = 'frontpage'; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-frontpage.php'); ?>

<!-- category listing -->
<div class="category-component">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">

				<div class="row">
					<div class="col-sm-12 ">
						<h2 class="title  js-mobile-expander" data-target=".categories.row">Find job per <span class="color--primary">kategori</span></h2>
						<hr>
					</div>
				</div>

				<div class="row  categories">
					<div class="col-sm-4 ">
						<ul>
							<li><a href="">Administration / Kontor</a></li>
							<li><a href="">Design &amp; Arkitektur</a></li>
							<li><a href="">Human Resources</a></li>
							<li><a href="">Ingeniør</a></li>
							<li><a href="">Jura</a></li>
							<li><a href="">Landbrug &amp; Fiskeri</a></li>
							<li><a href="">Markedsføring &amp; PR</a></li>
							<li><a href="">Medicinal / Biotek</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<ul>
						<li><a href="">Offentlige hverv</a></li>
						<li><a href="">Salg &amp; Indkøb</a></li>
						<li><a href="">Transport &amp; Logistik</a></li>
						<li><a href="">Økonomi, Regnskab &amp; Revision</a></li>
						<li><a href="">Bank, Finans &amp; Forsikring</a></li>
						<li><a href="">Hotel, Restaurant &amp; Turisme</a></li>
						<li><a href="">Industri, Håndværk &amp; Service</a></li>
						<li><a href="">IT</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<ul>
						<li><a href="">Kunst &amp; Kultur</a></li>
						<li><a href="">Ledelse</a></li>
						<li><a href="">Medicin, Sundhed &amp; Omsorg</a></li>
						<li><a href="">Medie &amp; Information</a></li>
						<li><a href="">Olieindustri &amp; on-/offshore</a></li>
						<li><a href=""> Sikkerhed &amp; Alarmberedskab</a></li>
						<li><a href="">Uddannelse / Børn &amp; Unge</a></li>
						</ul>
					</div>
				</div>

			</div>
			<div class="col-sm-3  hidden-xs">
				<div class="ad-block  ad-block--frontpage  secondary-content  narrow">
					<a href="">
						<img src="testcontent/temp-ad.jpg" alt="">
					</a>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- // category listing -->


<!-- the 4 image boxes with text and read-more link -->
<div class="imagelinkbox-component">
	<div class="container">

		<div class="row  header">
			<div class="col-sm-7 ">
				<h2 class="title">Gode råd til jobsøgning og karriere</h2>
			</div>
			<div class="col-sm-2  text-right  hidden-xs">
				<a href="">Se alle karriereråd</a>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-9"><hr></div>
		</div>

		<div class="row  mobile-carousel">
			<div class="mobile-carousel-holder">
				<div class="col-sm-3   imagelinkbox">
					<img src="testcontent/advice-1.jpg" alt="" class="img-responsive">
					<h4>Jobstrategi</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3  imagelinkbox">
					<img src="testcontent/advice-2.jpg" alt="" class="img-responsive">
					<h4>Dit nye job er kun 5 skridt væk!</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3  imagelinkbox">
					<img src="testcontent/advice-3.jpg" alt="" class="img-responsive">
					<h4>Jeg er ikke som de andre</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					<a href="">Læs mere</a>
				</div>
				<div class="col-sm-3">
					<!-- Frontpage overlap from ad-banner -->
					&nbsp;
				</div>
			</div>
		</div>

	</div>
</div>
<!-- //the 4 image boxes with text and read-more link -->


<!-- video and forum shortcuts -->
<div class="container  component--default">
	<div class="row">
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-6 ">
					<!-- alternative content (instead of video iframe) -->
					<div class="component  component--lightblue  popular-jobs">
						<h2>Populære job</h2>
						<hr>
						<ul id="newsticker" class="newsticker">
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56222&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/M/logoMercuri-Urval-5041JDK.gif" alt="Mercuri Urval">
									</div>
									<p>IT-informationsarkitekt til Femern A/S</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56216&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/H/logoHudson-1505JDK.gif" alt="Hudson Global Resources A/S">
									</div>
									<p>IT Konsulent til konsulenthus i vækst - København</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56276&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/F/logoFischer-Kerrn-A-S-3509JDK.gif" alt="Fischer &amp; Kerrn A/S">
									</div>
									<p>Serviceminded software supporter søges</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56249&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/W/logoWidex-A-S-1846JDK.gif" alt="Widex A/S">
									</div>
									<p>Hardwarenær softwareudvikler</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56156&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/f/4613.png" alt="Forsvarets Koncernfælles Informatiktjeneste">
									</div>
									<p>SQL-specialist (Ingeniør) I Forsvaret til støtte for de kæmpende enheder</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56193&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/M/logoME-Mover-Holding-ApS-12860JDK.gif" alt="ME-Mover Holding ApS">
									</div>
									<p>Social Media Internship</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56221&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/W/logoWhiteaway-A-S-12877JDK.gif" alt="Whiteaway A/S">
									</div>
									<p>Forretningsorienteret lead developer</p>
								</a>
							</li>
							<li>
								<a class="popularJob" href="http://www.it-jobbank.dk/index.cfm?event=offerView.dspOfferInline&amp;offerid=56276&amp;rewrite=1">
									<div class="logo">
										<img src="http://www.it-jobbank.dk/upload_IJ/logo/F/logoFischer-Kerrn-A-S-3509JDK.gif" alt="Fischer &amp; Kerrn A/S">
									</div>
									<p>Serviceminded software supporter søges</p>
								</a>
							</li>
						</ul>
					</div>
					<!-- end of alternative content  -->
				</div>
				<div class="col-sm-6">
					<div class="component--lightblue">

						<h2>Find dit jobfora</h2>

						<hr>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

						<div class="row">
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Salg og marketing</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ingeniør og teknik</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Økonomi og finans</a>
							</div>
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Medicinal og biotek</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ledelse</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">IT</a>
							</div>

							<a href="" class="btn  btn-primary  btn-md  btn-block  visible-xs-block">Find jobfora</a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">&nbsp;</div>
	</div>
</div>
<!-- // video and forum shortcuts -->


<!-- carousel -->
<?php require('_carousel.php'); ?>
<!-- // carousel -->


<?php require('_footer.php'); ?>