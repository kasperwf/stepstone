<?php
	$jsModule = ''; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-article.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">

			<!-- Menus and carousel -->
			<div class="row  hidden-xs">
				<div class="col-sm-12  carousel-and-menus">
					<div class="subfront-menus">

						<ul class="side-nav" role="navigation">
							<li><a href="">Styrker og kompetencer</a></li>
							<li><a href="">Jobstrategi og målsætninger</a>
							<li><a href="">Vil du være leder?</a></li>
							<li><a href="">Lønforhandling</a></li>
							<li><a href="">Konfliktløsning</a></li>
						</ul>

					</div>
					<?php include('_carousel-subfront.php'); ?>
				</div>
			</div>


			<!-- Small boxes -->
			<div class="image-boxes">
				<div class="row  imagelinkbox-component  mobile-carousel">
					<div class="row">
						<div class="col-sm-12"><hr></div>
					</div>
					<div class="mobile-carousel-holder">
						<div class="col-sm-4   imagelinkbox">
							<img src="testcontent/articles-3.jpg" alt="" class="img-responsive">
							<h4>Hvordan får du gang i en international karriere?</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
						<div class="col-sm-4  imagelinkbox">
							<img src="testcontent/articles-4.jpg" alt="" class="img-responsive">
							<h4>Skal du være ven med din chef på Facebook?</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
						<div class="col-sm-4   imagelinkbox">
							<img src="testcontent/article-new-related-1.jpg" alt="" class="img-responsive">
							<h4>Uligheden i Danmark er stigende</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
							<a href="">Læs mere</a>
						</div>
					</div>
				</div>
			</div>

			<!-- Four Steps -->
			<div class="row">
				<div class="col-sm-12">
					<div class="component--default">
						<h2>Guide til jobsøgning og karriere</h2>

						<ul class="four-steps">
							<li>
								<a href="">Jobsøgning</a>
								<ul>
									<li><a href="">Karrierevalg</a></li>
									<li><a href="">Brug dit netværk</a></li>
									<li><a href="">Jobstrategi</a></li>
								</ul>
							</li>
							<li>
								<a href="">Ansøgning</a>
								<ul>
									<li><a href="">Find din styrke</a></li>
									<li><a href="">En god ansøgning</a></li>
									<li><a href="">Tips til dit CV</a></li>
								</ul>
							</li>
							<li>
								<a href="">Jobsamtale</a>
								<ul>
									<li><a href="">Inden samtalen</a></li>
									<li><a href="">Selve samtalen</a></li>
									<li><a href="">Efter samtalen</a></li>
								</ul>
							</li>
							<li>
								<a href="">Ansættelse</a>
								<ul>
									<li><a href="">Løn</a></li>
									<li><a href="">Arbejdsopgaver</a></li>
									<li><a href="">Karriereudvikling</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<!-- video and forum shortcuts -->
			<div class="row">
				<div class="col-sm-6 ">
					<!-- video iframe -->
					<iframe width="380" height="213" src="//www.youtube.com/embed/QmsRgzfr0sk?fs=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;color=white&amp;theme=light" frameborder="0" allowfullscreen></iframe>
					<!-- end ofvideo iframe -->
				</div>
				<div class="col-sm-6">
					<div class="component--lightblue">

						<h2>Find dit jobfora</h2>

						<hr>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

						<div class="row">
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Salg og marketing</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ingeniør og teknik</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Økonomi og finans</a>
							</div>
							<div class="col-sm-6  hidden-xs">
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Medicinal og biotek</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">Ledelse</a>
								<a href="" class="btn  btn-primary  btn-sm  btn-block">IT</a>
							</div>

							<a href="" class="btn  btn-primary  btn-md  btn-block  visible-xs-block">Find jobfora</a>
						</div>

					</div>
				</div>
			</div>
			<!-- // video and forum shortcuts -->

		</div>
		<div class="col-sm-3  listing-sidebar-right  hidden-xs">
			<div class="secondary-content  narrow  frontpage  js-hideonscroll">
				<button class="btn btn--create-cv">Opret CV</button>
				<button class="btn btn--add-job">Opret job</button>

				<a href="">
					<img src="testcontent/temp-ad.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
</div>


<?php require('_footer.php'); ?>