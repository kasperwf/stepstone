<?php require("__head.php"); ?>

		<!-- Top bar (Langugage to the left, Feedback and Security links to the right) -->
		<div class="topbar  hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12   topbar-left">

					</div>
					<div class="col-sm-3">

						<div class="clearfix">

							<div class="pull-left">
								<a href="">Send feedback</a>
							</div>

							<div class="pull-right  text-right">
								<a href="">Sikkerhed</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end top bar -->

		<!-- Nav bar Logo || Nav -->
		<div class="navbar--custom  js-header  hidden-xs" role="navigation">

			<div class="container">
				<div class="row">

					<div class="col-sm-2">
						<a class="logo-holder" href="#">
							<img src="assets/images/logoX2.png" class="nav-logo  img-responsive">
						</a>
					</div>

					<div class="col-sm-10">
						<ul class="nav  js-nav--primary">
							<li class="active  current-page"><a href="#" data-id="jobsearch">Jobsøger</a></li>
							<li><a href="#" data-id="company">Virksomhed</a></li>
							<li><a href="#" data-id="about">Om os</a></li>
						</ul>
					</div>

					<div class="col-sm-3">
						<button class="btn btn--login">Log ind</button>
					</div>

				</div>
			</div>
			<div class="nav--secondary  js-nav--secondary">
				<div class="container">
					<div class="row">
						<div class="col-sm-15 ">

							<ul data-id="jobsearch">
								<li><a href="">Søg job</a></li>
								<li><a href="">Jobsøgning og karriere</a></li>
								<li><a href="">Jobfora</a></li>
								<li><a href="">Lorem ipsum</a></li>
								<li><a href="">Dolores est</a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end nav bar -->

		<!-- Search component -->
		<div class="search-component  collapsed  js-search  component--default   hidden-xs">
			<div class="container">
				<form class="form-horizontal" role="form" action="" method="">
					<div class="row  component-header  js-hideonscroll">
						<div class="col-sm-8 col-sm-offset-1">
							<div class="hide-on-collapsed">
								<h1>Find dit <span class="color--primary">karriere-job</span> her</h1>
							</div>
						</div>
						<div class="col-sm-4  hidden-xs">
							<div class="text-right  hide-on-collapsed">
								<button class="btn  btn-link  component-button--advanced  color--white"><span class="glyphicon  glyphicon-search  color--primary "></span> Avanceret søgning</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="tower-column  ad-only">
								<div class="secondary-content">
									<div class="ad-block">
										<a href="">
											<img src="testcontent/temp-ad.jpg" alt="">
										</a>
									</div>
								</div>

							</div>
						</div>

					</div>

					<div class="row  js-hideonscroll  hide-on-collapsed">
						<div class="col-sm-12   divider"><hr></div>
					</div>

					<div class="form-group  component-fields">
						<div class="col-sm-5 ">
							<label for="" class="control-label  text-left">Hvad</label>
							<input type="text" class="form-control" placeholder="Titel, virksomhed eller søgeord">
						</div>

						<div class="col-sm-5">
							<label for="" class="control-label  text-left">Hvor</label>
							<input type="text" class="form-control" placeholder="By eller postnummer">
						</div>

						<div class="col-sm-2">
							<button type="submit" class="btn  btn-success  btn-block">SØG</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- end search component -->
