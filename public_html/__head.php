<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title>StepStone</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="assets/css/screen.css">
</head>
<body>
	<!-- mobile nav -->
	<div class="navbar-header  visible-xs-block">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
		</button>
		<a class="navbar-brand" href="#"><img src="assets/images/logoX2.png"></a>
		<button type="button" class="navbar-login" data-toggle="modal" data-target="#loginModal">
			<span class="sr-only">Toggle navigation</span>
		</button>
	</div>
	<div class="navbar-collapse collapse  visible-xs-block" style="height:1px">
		<ul class="nav navbar-nav">
			<li class="active">
				<a href="#">Jobsøger</a>
				<ul>
					<li><a href="">Søg job</a></li>
					<li class="active">
						<a href="">Jobsøgning og karriere</a>
						<ul>
							<li><a href="">Vist</a></li>
							<li class="active"><a href="">Vist og aktiv</a></li>
							<li><a href="">Også vist</a></li>
						</ul>
					</li>
					<li>
						<a href="">Jobfora</a>
						<ul>
							<li><a href="">Skjult</a></li>
							<li><a href="">Skjult</a></li>
							<li><a href="">Skjult</a></li>
						</ul>
					</li>
					<li><a href="">Lorem ipsum</a></li>
					<li><a href="">Dolores est</a></li>
				</ul>
			</li>
			<li>
				<a href="#" >Virksomhed</a>
				<ul>
					<li><a href="">Skjult</a></li>
					<li><a href="">Skjult</a></li>
					<li><a href="">Skjult</a></li>
				</ul>
			</li>
			<li><a href="#" >Om os</a></li>
		</ul>
	</div>
	<!-- end of mobile nav -->

	<!-- login modal -->
	<form method="post">
	<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Luk</span></button>
						<h4 class="modal-title" id="myModalLabel">Login til Mit StepStone</h4>
					</div>
					<div class="modal-body">
						Formular her
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Annuller</button>
						<button type="submit" class="btn btn-primary">Login</button>
					</div>
			</div>
		</div>
	</div>
	</form>
	<!-- // login modal -->

	<div class="site-content">
