<div id="carousel-subfront" class="carousel slide  carousel-subfront" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-subfront" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-subfront" data-slide-to="1"></li>
		<li data-target="#carousel-subfront" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="testcontent/carousel-subfront-1.jpg" alt="">
			<div class="carousel-caption  no-shadow">
				<div class="statement">
					<h3>300.000</h3>
					<p>kandidater får ledige job tilsendt via email med StepStones Jobagent</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="testcontent/carousel-subfront-2.jpg" alt="">
			<div class="carousel-caption">
				<div class="statement">
					<h3>17 af 19</h3>
					<p>danskere kender godt fornemmelsen</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="testcontent/carousel-subfront-3.jpg" alt="">
			<div class="carousel-caption  no-shadow">
				<div class="statement">
					<h3>Railroads</h3>
					<p>Just going and going and going and going and going and ...</p>
				</div>
			</div>
		</div>

	</div>

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-subfront" role="button" data-slide="prev">
		<span class="carousel-nav  carousel-nav--left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-subfront" role="button" data-slide="next">
		<span class="carousel-nav  carousel-nav--right"></span>
	</a>
</div>