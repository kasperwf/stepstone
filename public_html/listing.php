<?php
	$jsModule = ''; // used for scripts in the footer
	$baseUrl = '/stepstone/public_html/';
?>
<?php require('_header-subpages.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">

			<div class="listing  content  component--default">
				<h1 class="title">Medarbejder til klargøring af køretøjer</h1>

				<div class="logo-and-info">
					<div class="company-logo">
						<img src="testcontent/tesla.png" alt="Tesla" class="img-responsive">
					</div>
					<div class="info">
						Randstad<br>København
					</div>
				</div>

				<div class="manchet">
					<p>Tesla Motors leder efter en medarbejder der vil være ansvarlig for at klargøre vores køretøjer til fremstilling, levering, og PR-arrangementer.<br>Arbejdet skal også give ekstra service til vores kunder via vores serviceafdeling. Vi er på udkig efter én der er pålidelig, opmærksom på detaljer, og som nyder at lave et godt stykke arbejde!</p>
				</div>

				<div class="section  component--default">
					<h2 class="subtitle">Opgaver</h2>
					<ul>
						<li>Hente og levere biler når det er nødvendigt</li>
						<li>Behandling af ad-hoc-opgaver</li>
						<li>Vask, lakering, og polering af køretøjer</li>
						<li>Fjernelse af ridser, samt vådsandblæsning</li>
						<li>Klargøring og støvsugning</li>
						<li>Anvendelse af autolakprodukter</li>
					</ul>
				</div>

				<div class="section  component--default">
					<h2 class="subtitle">Kompetencekrav</h2>
					<ul>
						<li>Vi foretrækker én med erfaring i lakering, polering og vådsandblæsning</li>
						<li>Gymnasial uddannelse, eller tilsvarende</li>
						<li>Være i stand til at løfte 22-27 kg</li>
					</ul>
				</div>

				<div class="section  company-about  component--lightgray ">
					<h2 class="subtitle">Om Virksomheden</h4>

					<p>Tesla Motors, som er hjemmehørende i Palo Alto, Californien, designer og producerer elektriske køretøjer og dertilhørende transmissionsdele. Teslas målsætning er at producere et komplet udvalg af elbiler fra sportsbiler til køretøjer til massemarkedet, og vi er fast besluttet på at reducere prisen på elektriske køretøjer. Vi har designet, produceret og leveret Tesla Roadsters til kunder i Nordamerika, Europa samt Asien og Stillehavsområdet. S-modellen, verdens første eldrevne kvalitetssedan, rulles ud nu over hele verden.</p>

					<a href="http://www.teslamotors.com/da_DK/" class="link">www.teslamotors.com/da_DK/</a>
				</div>

				<a href="" class="button">Søg jobbet her!</a>

				<a href="" class="btn--save-job visible-xs-block">Gem job</a>
			</div>

		</div>
		<div class="col-sm-3  listing-sidebar listing-sidebar--right">

			<div class="component--default logo-date-and-listing-actions hidden-xs">
				<div class="company-logo">
					<img src="testcontent/tesla.png" alt="Tesla">
				</div>

				<p class="published">Publiceret: <strong>11. juli 2014</strong></p>

				<div class="listing-buttons">
					<a href="" class="btn--save-job">Gem job</a>
				</div>
			  </div>

			<div class="job-location  component--lightblue">
				<p class="title">Arbejdssted</p>
				<!-- Copy paste embed SRC from Google Maps -->
				<iframe
					width="182"
					height="160"
					frameborder="0"
					style="border: 0"
					src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDc9DicjnUI96OMvLWASL0BM0_CsFWevtE&amp;q=København&amp;language=da"
				></iframe>
				<!-- <img src="testcontent/map.png" alt="" class="img-responsive"> -->
				<p class="city">København</p>
			</div>

			<div class="company-other-jobs  component--lightblue">
				<p class="title">Jobs hos virksomheden</p>
				<ul>
					<li><a href="">Regional MarketingAssociate</a></li>
					<li><a href="">HR Administrator</a></li>
					<li><a href="">Graduate Recruitment Coordinator, EMEA</a></li>
				</ul>
			</div>

			<div class="share">
				<span>Del job:</span>
				<img src="assets/images/share-mail.png" alt="">
				<img src="assets/images/share-facebook.png" alt="">
				<img src="assets/images/share-linkedin.png" alt="">
				<img src="assets/images/share-gplus.png" alt="">
				<img src="assets/images/share-twitter.png" alt="">
			</div>

		</div>
	</div>
</div>


<?php require('_footer.php'); ?>