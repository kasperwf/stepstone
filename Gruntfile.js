module.exports = function(grunt) {
	require('load-grunt-config')(grunt);
	grunt.registerTask('default', ['bowercopy', 'copy:main', 'imagemin','newer:jshint','less:dev','newer:uglify:dev', 'watch']);
	grunt.registerTask('compile', ['bowercopy', 'copy:main', 'imagemin','newer:jshint','less:compile','newer:uglify:compile']);
};