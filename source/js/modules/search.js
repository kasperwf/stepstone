(function($) {
	$('#toggleAgent').click(function(e) {
		var $this = $(this),
			$createAgent = $('#createAgent');

		if ($createAgent.is(':visible')) {
			$createAgent.slideUp();
			$this.addClass('collapsed');
		} else {
			$createAgent.slideDown(function() {
				$createAgent.find('.email').focus();
				$this.removeClass('collapsed');
			});
		}
	});

	if ($(window).width() >  480) {
		// Desktop - disable collapsables in left sidebar
		var $filters = $('#filtersholder');
		$filters.find('.title[data-toggle="collapse"]').off('click').removeClass('collapsed').on('click', function(e) {
			e.stopPropagation();
			e.preventDefault();
			return false;
		});
		console.log($filters.find('.panel-collapse'));
		$filters.find('.panel-collapse').removeClass('collapse in');

	}
})(jQuery);