(function($) {
    $(function() {
	if ($(window).width() <=  480) {
		// Mobile
		$('.js-mobile-expander').on('click', function(e) {
			e.preventDefault();
			$($(this).data('target')).toggleClass('expanded');
		});
	}

	// Newsticker
	var newsticker = $('#newsticker').newsTicker({
	    row_height: 48,
	    max_rows: 3,
	    duration: 4000,
	    speed: 1000
	});

   });
})(jQuery);
