(function($) {

	var headerSpy = function() {
		var $header = $('.js-header'),
			$header_clone = $header.clone().addClass('clone'),
			$search = $('.js-search'),
			$search_clone = $search.clone().addClass('clone'),
			$body = $('body');

		$header_clone.find('.js-nav--secondary').remove();
		$header_clone.css('top', '-1000px');

		$search_clone.css('top', '-1000px');

		$body.prepend($search_clone,$header_clone);

		var headerOuterHeight = $header_clone.outerHeight(true),
			searchOuterHeight = $search_clone.outerHeight(true),
			headerLimit = $header.offset().top + $header.outerHeight(true),
			searchLimit = headerLimit + 80;

		$header_clone.css('top', '-' + headerOuterHeight + 'px').data({
			'default-position': '-' + headerOuterHeight + 'px',
			'fixed-position': '0'
		});

		$search_clone.css('top', '-' + searchOuterHeight + 'px').data({
			'default-position':  '-' + searchOuterHeight + 'px',
			'fixed-position': headerOuterHeight + 'px'
		});

		$(window).on('scroll', function() {

			var fromTop = $body.scrollTop();

			// header
			if (fromTop > headerLimit) {
				$header_clone.css('top', $header_clone.data('fixed-position'));
			} else {
				$header_clone.css('top', $header_clone.data('default-position'));
			}

			// search
			if (fromTop > searchLimit) {
				$search_clone.css('top', $search_clone.data('fixed-position'));
			} else {
				$search_clone.css('top', $search_clone.data('default-position'));
			}

		});

	};


	var mainSubNav = {
		options: {
			durationBeforeRevert: 5000 // miliseconds
		},
		init: function() {
			this.primary = $('.js-nav--primary');
			this.secondary = $('.js-nav--secondary');

			this.current_page = this.primary.find('.current-page');
			this.timer = "";
			this.current_page_is_active = true;

			this._bindEvents();
			this._processActive();
		},
		openSecondary: function(_id) {
			var subnav = this.secondary.find('ul[data-id="' + _id + '"]');

			if (subnav.length) {
				this.secondary.addClass('active');
				subnav.addClass('active');
			}
		},
		_bindEvents: function() {
			var _this = this;
			this.primary.on('click','a', function(e) {
				_this._click(e, $(this));
			});
			this.primary.on({
				'mouseover': function() {
					_this.clearTimer();
				},
				'mouseleave': function() {
					_this.startTimer();
				}
			});
			this.secondary.on({
				'mouseover': function() {
					_this.clearTimer();
				},
				'mouseleave': function() {
					_this.startTimer();
				}
			});
		},
		startTimer: function() {
			if (this.current_page_is_active) {
				return;
			}
			var _this = this;
			this.timer = window.setTimeout(function() {
				_this._revert();
			}, this.options.durationBeforeRevert);
		},
		clearTimer: function() {
			window.clearTimeout(this.timer);
		},
		_click: function(e, obj) {
			e.preventDefault();
			this.primary.find('li').not(obj.closest('li')).removeClass('active');
			obj.closest('li').addClass('active');

			this.current_page_is_active = (obj.closest('li').hasClass('current-page'));

			this._processActive();
		},
		_processActive: function() {
			// Hide the secondary nav first
			this.secondary.find('ul').removeClass('active');
			this.secondary.removeClass('active');

			var active = this.primary.find('li.active');
			if (active.length) {
				this.openSecondary(active.find('a:first').data('id'));
			}
		},
		_revert: function() {
			this.current_page.find('a:first').click();
		}
	};

	if ($(window).width() >  480) {
		mainSubNav.init();
		headerSpy();
	}

})(jQuery);