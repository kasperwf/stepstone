module.exports = {
	dev: {
		options: {
			sourceMap: true,
			mangle: true,
			beautify: false,
			compress: true
		},
		files: [
			{
				expand: true,
				cwd: 'source/js/',
				src: ['**/*.js'],
				dest: 'public_html/assets/js/',
				ext: '.js'
			}
		]
	},
	compile: {
		options: {
			sourceMap: false,
			mangle: true,
			beautify: false,
			compress: true,
			preserveComments: false
		},
		files: [
			{
				expand: true,
				cwd: 'source/js/',
				src: ['**/*.js'],
				dest: 'public_html/assets/js/',
				ext: '.js'
			}
		]
	}
};