module.exports = {
	dist: {
		options: {
			style: 'compact',
			sourcemap: true
		},
		files: [{
			'public_html/assets/css/screen.css': 'source/sass/screen.scss',
			'public_html/assets/css/shame.css': 'source/sass/shame.scss'
		}]
	},
	compile: {
		options: {
			style: 'compressed',
			sourcemap: false,
			quiet: true
		},
		files: [{
			'public_html/assets/css/screen.css': 'source/sass/screen.scss',
			'public_html/assets/css/shame.css': 'source/sass/shame.scss',
			'public_html/assets/css/print.css': 'source/sass/print.scss',
			'web/css/admin.css': 'source/sass/admin.scss'
		}]
	}
};