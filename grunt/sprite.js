module.exports = {
	packed: {
		src: 'source/sprite-input/packed/*.png',
		destImg: 'source/images/spritesheet.png',
		destCSS: 'source/sass/mixins/spritesheets/_spritesheet.scss',
		imgPath: '../images/spritesheet.png',
		algorithm: 'binary-tree'
	}
	/*vertical: {
		src: 'source/sprite-input/vertical/*.png',
		destImg: 'source/images/sprite/spritesheet-v.png',
		destCSS: 'source/less/mixins/spritesheets/spritesheet-v.less',
		imgPath: '../images/sprite/spritesheet-v.png',
		algorithm: 'top-down'
	},
	horizontal: {
		src: 'source/sprite-input/horizontal/*.png',
		destImg: 'source/images/sprite/spritesheet-h.png',
		destCSS: 'source/less/mixins/spritesheets/spritesheet-h.less',
		imgPath: '../images/sprite/spritesheet-h.png',
		algorithm: 'left-right'
	}*/
};