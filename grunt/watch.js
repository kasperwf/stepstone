module.exports = {
	styles: {
		files: ['source/less/**/*.less'],
		tasks: ['less:dev'],
		options: {
			spawn: false,
			interrupt: true,
		},
	},
	scripts: {
		files: ['source/js/**/*.js', '!source/js/vendor/**/*.js'],
		tasks: ['newer:jshint', 'newer:uglify'],
		options: {
			spawn: false,
			interrupt: false,
		},
	},
	options: {
		livereload: true
	}
};