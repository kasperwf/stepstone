module.exports = {
	main: {
		expand: true,
		src: ['source/fonts/*'],
		dest: 'public_html/assets/fonts',
		flatten: true,
		filter: 'isFile'
	}
};