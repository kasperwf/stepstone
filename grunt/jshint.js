module.exports = {
	files: ['!Gruntfile.js', '!public_html/assets/js/**/*.js', 'source/js/**/*.js', '!source/js/vendor/**/*.js'],
	options: {
		curly: true,
		eqeqeq: true,
		immed: true,
		latedef: true,
		newcap: true,
		noarg: true,
		sub: true,
		undef: true,
		boss: true,
		eqnull: true,
		browser: true,

		ignores: [],
		globals: {
			module: true,
			require: true,
			requirejs: true,
			jQuery: true,
			google: true,
			console: true,
			define: true,
			BASEURL: true
		}
	}
};