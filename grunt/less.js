module.exports = {
	dev: {
		options: {
			// cleancss: true, // removes
			compact: true,
			sourceMap: true,
			sourceMapFilename: 'public_html/assets/css/screen.css.map', // where file is generated and located
            sourceMapURL: '/stepstone/public_html/assets/css/screen.css.map', // the complete url and filename put in the compiled css file
            sourceMapBasepath: 'public_html', // Sets sourcemap base path, defaults to current working directory.
            sourceMapRootpath: '/stepstone/', // a
		},
		files: {
			'public_html/assets/css/screen.css': 'source/less/screen.less',
			'public_html/assets/css/shame.css': 'source/less/shame.less'
		}
	},
	compile: {
		options: {
			cleancss: true, // removes
			compact: true,
			sourceMap: false
		},
		files: {
			'public_html/assets/css/screen.css': 'source/less/screen.less',
			'public_html/assets/css/shame.css': 'source/less/shame.less'
		}
	}
};