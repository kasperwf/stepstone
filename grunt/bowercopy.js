module.exports = {
	options: {
		// Task-specific options go here
	},
	libs: {
		options: {
			destPrefix: 'public_html/assets/js/vendor'
		},
		files: {
			'jquery.min.js': 'jquery/dist/jquery.min.js'
		},
	},
	// bootstrap: {
	// 	options: {
	// 		destPrefix: 'public_html/assets/css'
	// 	},
	// 	files: {
	// 		'vendor/bootstrap.min.css': 'bootstrap/dist/css/bootstrap.min.css'
	// 	}
	// },
	bootstrap_fonts: {
		options: {
			destPrefix: 'public_html/assets/fonts'
		},
		files: {
			'': 'bootstrap/dist/fonts/*'
		}
	},
	bootstrap_js: {
		options: {
			destPrefix: 'public_html/assets/js/vendor'
		},
		files: {
			'bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js'
		},
	}
};